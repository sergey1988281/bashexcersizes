#!/bin/bash
#
#Removes all stopped containers of given image
#
#Input parameters - image name
#
#Example: bash remove_containers.sh nginx
#

if
    (($# != 1))
then
    echo "Command accepts exactly 1 argument"
    exit 1
fi

echo "Removing all $1 containers in \"Exited\" state"
for container in $(docker ps -a | awk '/'"$1"'.+Exited/{print $1}')
do
    docker container rm $container
done
