#!/bin/bash
#
# Spawns HTTP workers as deattached jobs using generate_http_requests.sh script which it expects in the same directory
# Script interactively asks to provide all parameters and then execute workload until cancelled
#

generator_script="./generate_http_requests.sh"
if [ ! -f $generator_script ]; then
    echo "$generator_script does not exist."
    exit 1
fi

urls=()
worker_counts=()
delays=()
valid_url='(https?|ftp|file)://[-[:alnum:]\+&@#/%?=~_|!:,.;]*[-[:alnum:]\+&@#/%=~_|]'
valid_number='^[0-9]*$'
while true; do
	echo "Enter URL to generate workload:"
	read url
	if [[ ! $url =~ $valid_url ]]
        then 
        	echo "URL is invalid, try again"
		continue
	fi
	echo "How many workers to spawn:"
	read worker_count
	if [[ ! $worker_count =~ $valid_number ]]
	then
		echo "Worker count must be a number, try again"
		continue
	fi
	echo "Specify maximum delay between requests in seconds"
	read delay
        if [[ ! $delay =~ $valid_number ]]
        then
                echo "Delay must be a number, try again"
                continue
        fi
	urls+=($url)
	worker_counts+=($worker_count)
	delays+=($delay)
        read -p "More urls?(y/n)" yn
	case $yn in
        	y ) continue;;
                * ) break;
        esac
done
for i in ${!urls[@]}; do
	for j in $(seq ${worker_counts[$i]}); do
		$generator_script ${urls[$i]} ${delays[$i]} &
	done
done
sleep 5
echo "Generated processes with ids:" 
jobs -p
read -p "Press enter to stop" finish
kill $(jobs -p)
