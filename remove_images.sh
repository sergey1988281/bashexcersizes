#!/bin/bash
#
#Removes all images given name and tag
#
#Input parameters - image name, image tag
#
#Example: bash remove_images.sh "<none>" "<none>""
#

if
    (($# != 2))
then
    echo "Command accepts exactly 2 arguments: image and tag"
    exit 1
fi

echo "Removing all $1 images with $2 tag"
for image in $(docker image ls | awk '/'"$1"'.+'"$2"'/{print $3}')
do
    docker image rm $image
done
