#!/bin/bash


echo "Welcome to bash calculator"
while true
do
	echo "Enter expression"
	read expression
	if [[ $expression == "exit" ]]
	then
		echo "bye"
		exit 0
	fi
	valid_input='^[0-9]+ +([+\-\/%]|\*{1,2}) +[0-9]+$'
	if [[ $expression =~ $valid_input ]]
	then
		let "result=$expression"
		echo "$result"
	else
		echo "error" >&2
		exit 1
	fi
done
