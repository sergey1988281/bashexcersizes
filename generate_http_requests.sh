#!/bin/bash

# Script infinitely generates http workload to a given URL with random delay
# $1 - url to generate workload
# $2 - maximum value for delay (default is 5 seconds)

if [ -z ${2+x} ]; then
	delay=5
	default=" default value"
else
	delay=$2
fi

echo "Starting workload generation for $1 with random delay between 0 and $delay$default seconds"
while :
do
	curl -k $1 &> /dev/null
	sleep $(( $RANDOM % ( $delay + 1) ))
done
