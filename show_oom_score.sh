#!/bin/bash
#
# Dispays currently running processes and their oom_scores

while read -r pid comm;
do
	printf '%d\t%d\t%s\n' "$pid" "$(cat /proc/$pid/oom_score)" "$comm"
done < <(ps -e -o pid= -o comm=)
